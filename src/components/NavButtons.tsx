import * as React from 'react';
import { Theme, WithStyles, withStyles, Button , Icon } from '@material-ui/core';
import { CSSProperties } from '@material-ui/core/styles/withStyles';

export interface NavButtonProps extends WithStyles<typeof styles> {
    icon: string;
}

const styles = (theme: Theme): Record<any, CSSProperties> => ({
    button: {
        maxWidth: '36px',
        maxHeight: '36px',
        minWidth: '36px',
        minHeight: '36px',
        borderColor: "#4c83ff",
        borderRadius: '3px',
        margin: '1px'
    },
    input: {
      display: 'none',
    },
    icon: {
        color: "#4c83ff",
      },
      iconHover: {
        '&:hover': {
          backgroundColor: "#4c83ff",
          color: "white"
        },
      },
  });

  function NavButtons(props: NavButtonProps): JSX.Element {
    const { classes,  icon } = props;
    return (
        <div>
            <Button variant="outlined" className={classes.button}>
                <Icon className={classes.icon}>
                    {icon}
                </Icon>
            </Button>
        </div>
    )
  }

  export default withStyles(styles)(NavButtons);