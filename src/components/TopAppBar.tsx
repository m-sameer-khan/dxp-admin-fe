import * as React from 'react';
import { SetStateAction, Dispatch } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import Breadcrumbs from '@material-ui/lab/Breadcrumbs';
import {
  Icon,
  WithStyles,
  withStyles,
  createStyles,
  Theme,
  Link,
  Typography,
  Switch,
} from '@material-ui/core';

import UserButton from './UserButton';

import someLogo from '../quadient_logo_inv.png';

import logo from '../quadient_logo.svg';
// const logo =
//   'https://www.metlife.com/etc/designs/us/_jcr_content/global/logo/image.asset.spool/MetLife.png';

export interface ITopAppBarProps extends WithStyles<typeof styles> {
  activeTab: string;
  isDark: boolean;
  setDark: Dispatch<SetStateAction<boolean>>;
}

const button = {
  minWidth: 36,
  minHeight: 36,
  maxWidth: 36,
  maxHeight: 36,
};

const styles = (theme: Theme) =>
  createStyles({
    logo: {
      height: '28px',
      paddingLeft: 20,
      paddingRight: 100,
    },
    appBar: {
      background: theme.palette.background.default,
      color: theme.palette.primary.main,
      fontFamily: 'Lato',
      height: '6vh',
    },
    button: {
      color: theme.palette.primary.light,
      marginLeft: 'auto',
      ...button,
      marginRight: 10,
    },
    navButton: {
      color: theme.palette.primary.light,
      marginLeft: 5,
      ...button,
    },
    buttonIcon: {
      color: theme.palette.primary.main,
    },
    breadcrumb: {
      color: theme.palette.secondary.main,
      paddingLeft: 20,
      fontWeight: 'bold',
      textTransform: 'uppercase',
    },
  });

function getBreadCrumb(
  activeTab: string,
  breadcrumb: string,
): JSX.Element | null {
  return activeTab === 'Tenants' ? null : (
    <Breadcrumbs separator="›" className={breadcrumb} aria-label={activeTab}>
      <Link color="primary" href="/">
        Home
      </Link>
      <Typography
        color="secondary"
        style={{ textTransform: 'uppercase', fontWeight: 'bold' }}
      >
        {activeTab}
      </Typography>
    </Breadcrumbs>
  );
}

function navButton(
  buttonIcon: string,
  buttonStyle: string,
  buttonIconStyle: string,
): JSX.Element {
  return (
    <Button
      key={buttonIcon}
      aria-owns={open ? 'simple-menu' : undefined}
      variant="outlined"
      color="inherit"
      className={buttonStyle}
    >
      <Icon className={buttonIconStyle}>{buttonIcon}</Icon>
    </Button>
  );
}

function TopAppBar(props: ITopAppBarProps): JSX.Element {
  const { classes, activeTab, isDark, setDark } = props;

  return (
    <AppBar position="static" className={classes.appBar}>
      <Toolbar>
        <img
          src={isDark ? someLogo : logo}
          alt="Quadient"
          className={classes.logo}
        />

        {activeTab === 'Tenants'
          ? []
          : [
              navButton(
                'keyboard_arrow_left',
                classes.navButton,
                classes.buttonIcon,
              ),
              navButton(
                'keyboard_arrow_right',
                classes.navButton,
                classes.buttonIcon,
              ),
            ]}
        {getBreadCrumb(activeTab, classes.breadcrumb)}

        <Button
          aria-owns={open ? 'simple-menu' : undefined}
          variant="outlined"
          color="inherit"
          className={classes.button}
        >
          <Icon className={classes.buttonIcon}>search</Icon>
        </Button>
        <Switch
          color="primary"
          checked={isDark}
          onChange={() => setDark(!isDark)}
          value={false}
        />
        <UserButton />
      </Toolbar>
    </AppBar>
  );
}

export default withStyles(styles)(TopAppBar);
