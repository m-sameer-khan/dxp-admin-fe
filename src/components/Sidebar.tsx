import * as React from "react";
import { Dispatch, SetStateAction } from "react";
import {
  withStyles,
  WithStyles,
  Paper,
  List,
  ListItemIcon,
  Icon,
  Theme,
  createStyles,
  MenuItem
} from "@material-ui/core";

export interface ISidebarProps extends WithStyles<typeof styles> {
  activeTab: string;
  onPress: Dispatch<SetStateAction<string>>;
}

export interface IMenuItemProps {
  classes: Record<
    "menuItem" | "icon" | "primary" | "iconSelected" | "textSelected",
    string
  >;
  icon: string;
  heading: string;
  onClick: () => void;
  selected: boolean;
}

const styles = (theme: Theme) =>
  createStyles({
    root: {
      background: theme.palette.background.default,
      flex: 3,
      maxWidth: 250,
      minWidth: 250,
      paddingTop: 40,
      padding: "0 15 15 15",
      borderRight: `1px solid ${theme.palette.secondary.light}`,
      borderTop: `1px solid ${theme.palette.secondary.light}`,
      boxShadow: "none"
    },
    menuItem: {
      padding: 20,
      textTransform: "uppercase",
      "& $primary, & $icon": {
        color: theme.palette.text.hint
      }
    },
    icon: {
      paddingLeft: 20
    },
    primary: {
      fontWeight: "bold",
      fontSize: 14
    },
    iconSelected: {
      paddingLeft: 20,
      color: theme.palette.text.primary
    },
    textSelected: {
      color: theme.palette.text.primary,
      fontWeight: "bold"
    }
  });

const GenMenuItem = (props: IMenuItemProps) => (
  <MenuItem
    selected={props.selected}
    onClick={props.onClick}
    className={props.classes.menuItem}
  >
    <ListItemIcon
      className={
        props.selected ? props.classes.iconSelected : props.classes.icon
      }
    >
      <Icon>{props.icon}</Icon>
    </ListItemIcon>
    <div
      className={
        props.selected ? props.classes.textSelected : props.classes.primary
      }
    >
      {props.heading}
    </div>
  </MenuItem>
);

const drawer = (
  classes: Record<any, string>,
  current: string,
  setCurrent: React.Dispatch<React.SetStateAction<string>>
): JSX.Element => (
  <List>
    <GenMenuItem
      selected={current === "Tenants"}
      onClick={() => setCurrent("Tenants")}
      classes={classes}
      icon="home"
      heading="Tenants"
    />
    <GenMenuItem
      selected={current === "Users"}
      onClick={() => setCurrent("Users")}
      classes={classes}
      icon="person"
      heading="Users"
    />
    <GenMenuItem
      selected={current === "Roles"}
      onClick={() => setCurrent("Roles")}
      classes={classes}
      icon="people"
      heading="Roles"
    />
    <GenMenuItem
      selected={current === "Reports"}
      onClick={() => setCurrent("Reports")}
      classes={classes}
      icon="assignment"
      heading="Reports"
    />
    <GenMenuItem
      selected={current === "Insights"}
      onClick={() => setCurrent("Insights")}
      classes={classes}
      icon="grade"
      heading="Insights"
    />
  </List>
);

function Sidebar(props: ISidebarProps): JSX.Element {
  const { classes, activeTab, onPress } = props;

  return (
    <Paper className={classes.root}>
      {drawer(classes, activeTab, onPress)}
    </Paper>
  );
}

export default withStyles(styles)(Sidebar);
