import * as React from "react";
import { withStyles, WithStyles, Theme, createStyles } from "@material-ui/core";

export interface ISidebarProps extends WithStyles<typeof styles> {}

const styles = (theme: Theme) =>
  createStyles({
    root: {
      flex: 1,
      background: theme.palette.background.paper,
      height: "100vh",
      color: theme.palette.text.hint
    }
  });

function Sidebar(props: ISidebarProps): JSX.Element {
  const { classes } = props;

  return <div className={classes.root}>Graphs will go here!</div>;
}

export default withStyles(styles)(Sidebar);
