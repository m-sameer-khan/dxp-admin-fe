import * as React from "react";
import { withStyles, WithStyles, createStyles, Theme } from "@material-ui/core";

export interface IUserProps extends WithStyles<typeof styles> {}

const styles = (theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
      marginBottom: "auto"
    },
    panel1: {
      flex: 1
    },
    panel2: {
      flex: 1
    },
    text: {
      color: theme.palette.text.hint,
      fontSize: 40,
      paddingTop: 10
    }
  });

function Users(props: IUserProps): JSX.Element {
  const { classes } = props;
  return (
    <div className={classes.root}>
      <div className={classes.panel1}>
        <div className={classes.text}>Users</div>
      </div>
      <div className={classes.panel2}>
        <div>Second panel</div>
      </div>
    </div>
  );
}

export default withStyles(styles)(Users);
