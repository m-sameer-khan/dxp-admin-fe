import * as React from "react";
import { withStyles, WithStyles, createStyles, Theme } from "@material-ui/core";

export interface IRoleProps extends WithStyles<typeof styles> {}

const styles = (theme: Theme) =>
  createStyles({
    text: {
      color: theme.palette.text.hint,
      fontSize: 40,
      paddingTop: 10
    }
  });

function Roles(props: IRoleProps): JSX.Element {
  const { classes } = props;
  return <div className={classes.text}>Roles</div>;
}

export default withStyles(styles)(Roles);
