import * as React from "react";
import {
  withStyles,
  WithStyles,
  createStyles,
  Fab,
  Theme
} from "@material-ui/core";

import TenantDetails from "./TenantDetails";

export interface ITenantsProps extends WithStyles<typeof styles> {}

const styles = (theme: Theme) =>
  createStyles({
    fab: {
      // margin: 10,
      float: "right",
      backgroundColor: theme.palette.primary.main,
      padding: "0 30px",
      color: "#ffffff",
      fontWeight: "bolder",
      marginTop: 5
    },
    text: {
      color: theme.palette.text.hint,
      fontSize: 40,
      paddingTop: 10
    },
    tenantInfo: {
      display: "flex",
      paddingTop: 40
    }
  });

function Tenants(props: ITenantsProps): JSX.Element {
  const { classes } = props;

  return (
    <div className={classes.text}>
      Tenants
      <Fab variant="extended" aria-label="Create" className={classes.fab}>
        CREATE
      </Fab>
      <div className={classes.tenantInfo}>
        <TenantDetails />
      </div>
    </div>
  );
}

export default withStyles(styles)(Tenants);
