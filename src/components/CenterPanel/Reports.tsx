import * as React from "react";
import { withStyles, WithStyles, createStyles, Theme } from "@material-ui/core";

export interface IReportProps extends WithStyles<typeof styles> {}

const styles = (theme: Theme) =>
  createStyles({
    text: {
      color: theme.palette.text.hint,
      fontSize: 40,
      paddingTop: 10
    }
  });

function Reports(props: IReportProps): JSX.Element {
  const { classes } = props;
  return <div className={classes.text}>Reports</div>;
}

export default withStyles(styles)(Reports);
