import * as React from "react";
import { Theme, createStyles, withStyles, WithStyles } from "@material-ui/core";

import Tenants from "./Tenants";
import Users from "./Users";
import Roles from "./Roles";
import Reports from "./Reports";
import Insights from "./Insights";

export interface ICenterPanelProps extends WithStyles<typeof styles> {
  activeTab: string;
}

const styles = (theme: Theme) =>
  createStyles({
    root: {
      background: theme.palette.background.default,
      flex: 15,
      padding: 35,
      borderTop: `1px solid ${theme.palette.secondary.light}`
    }
  });

function getActiveTab(activeTab: string, root: string): JSX.Element {
  switch (activeTab) {
    case "Tenants":
      return <Tenants />;
    case "Users":
      return <Users />;
    case "Roles":
      return <Roles />;
    case "Reports":
      return <Reports />;
    case "Insights":
      return <Insights />;
    default:
      return <Tenants />;
  }
}

function CenterPanel(props: ICenterPanelProps): JSX.Element {
  const {
    classes: { root },
    activeTab
  } = props;

  return <div className={root}>{getActiveTab(activeTab, root)}</div>;
}

export default withStyles(styles)(CenterPanel);
