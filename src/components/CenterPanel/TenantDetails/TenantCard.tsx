import * as React from "react";
import {
  WithStyles,
  Theme,
  createStyles,
  withStyles,
  Card,
  CardContent,
  Typography,
  CardActions,
  Button,
  Icon
} from "@material-ui/core";

export interface ITenantCardProps extends WithStyles<typeof styles> {
  companyName: string;
  type: string;
  email: string;
}

const styles = (theme: Theme) =>
  createStyles({
    card: {
      flex: 1,
      minWidth: 250,
      minHeight: 250,
      textAlign: "center",
      background: theme.palette.background.paper,
      margin: 10
    },
    title: {
      fontSize: 16,
      color: theme.palette.secondary.main,
      paddingTop: 12,
      fontWeight: "bold"
    },
    pos: {
      marginBottom: 12
    },
    typeButton: {
      padding: 5,
      color: theme.palette.text.hint,
      fontSize: 12,
      margin: 0,
      textTransform: "uppercase"
    },
    email: {
      fontSize: 14,
      paddingTop: 25,
      color: theme.palette.text.hint,
      paddingBottom: 20
    },
    actions: {
      display: "flex",
      padding: 0
    },
    actionItem: {
      flex: 1,
      fontSize: 10,
      border: `1px solid ${theme.palette.secondary.light}`,
      padding: 10,
      margin: 0
    }
  });

function TenantCard(props: ITenantCardProps): JSX.Element {
  const { classes, companyName, type, email } = props;

  return (
    <Card className={classes.card}>
      <CardContent>
        <Typography className={classes.title} gutterBottom>
          {companyName}
        </Typography>
        <Button className={classes.typeButton} variant="outlined">
          {type}
        </Button>
        <Typography className={classes.email} component="p">
          {email}
        </Typography>
      </CardContent>
      <CardActions className={classes.actions}>
        <div className={classes.actionItem}>
          <Icon>select_all</Icon>
          <div>94%</div>
        </div>
        <div className={classes.actionItem}>
          <Icon>av_timer</Icon>
          <div>20%</div>
        </div>
        <div className={classes.actionItem}>
          <Icon>grade</Icon>
          <div>77</div>
        </div>
        <div className={classes.actionItem}>
          <Icon>person</Icon>
          <div>19216</div>
        </div>
      </CardActions>
    </Card>
  );
}

export default withStyles(styles)(TenantCard);
