import * as React from "react";
import {
  withStyles,
  WithStyles,
  Theme,
  createStyles,
  Icon
} from "@material-ui/core";
import TenantCard from "./TenantCard";

export interface ITenantDetailsProps extends WithStyles<typeof styles> {}

const styles = (theme: Theme) =>
  createStyles({
    main: {
      fontSize: 17,
      color: theme.palette.primary.main
    },
    icon: {
      minWidth: 30,
      maxWidth: 30,
      minHeight: 30,
      maxHeight: 30,
      paddingTop: 5,
      position: "relative",
      top: theme.spacing.unit
    },
    tenantCards: {
      padding: 25,
      display: "flex",
      flexWrap: "wrap",
      justifyContent: "space-between"
    }
  });

function TenantDetails(props: ITenantDetailsProps): JSX.Element {
  const { classes } = props;

  return (
    <div>
      <div className={classes.main}>
        <Icon className={classes.icon}>list</Icon>Switch to List View
      </div>
      <div className={classes.tenantCards}>
        <TenantCard
          companyName="Persha Company"
          type="Insurance"
          email="admin@persha.com"
        />
        <TenantCard
          companyName="NY Life"
          type="Insurance"
          email="admin@nylife.com"
        />
        <TenantCard
          companyName="MetLife"
          type="Banking"
          email="admin@metlife.com"
        />
        <TenantCard
          companyName="ByeLife"
          type="Industry"
          email="admin@byelife.com"
        />
        <TenantCard
          companyName="Providna"
          type="Banking"
          email="admin@providna.com"
        />
      </div>
    </div>
  );
}

export default withStyles(styles)(TenantDetails);
