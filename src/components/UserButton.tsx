import * as React from "react";
import { useState } from "react";
import {
  withStyles,
  WithStyles,
  createStyles,
  Button,
  Menu,
  MenuItem,
  Avatar,
  Icon,
  Theme
} from "@material-ui/core";

import user1 from "../face1.png";

const style = (theme: Theme) =>
  createStyles({
    userButton: {
      color: theme.palette.primary.light,
      marginLeft: 8,
      height: 36
    },
    avatar: {
      height: 20,
      width: 20,
      marginRight: 10
    },
    dropIcon: {
      marginLeft: 5,
      color: theme.palette.text.hint
    },
    black: {
      color: theme.palette.text.hint,
      textTransform: "none",
      fontWeight: "bold"
    }
  });

export interface IUserButtonProps extends WithStyles<typeof style> {}

function UserButton(props: IUserButtonProps): JSX.Element {
  const { classes } = props;
  const [anchorEl, setAnchorEl] = useState<null | EventTarget & HTMLElement>(
    null
  );

  return (
    <React.Fragment>
      <Button
        variant="outlined"
        color="inherit"
        className={classes.userButton}
        aria-haspopup="true"
        onClick={e => setAnchorEl(e.currentTarget)}
      >
        <Avatar alt="Remy Sharp" src={user1} className={classes.avatar} />
        <div className={classes.black}>Mike</div>
        <Icon className={classes.dropIcon}>expand_more</Icon>
      </Button>
      <Menu
        id="simple-menu"
        open={!!anchorEl}
        onClose={() => setAnchorEl(null)}
        anchorEl={anchorEl}
      >
        <MenuItem onClick={() => setAnchorEl(null)}>Profile</MenuItem>
        <MenuItem onClick={() => setAnchorEl(null)}>My account</MenuItem>
        <MenuItem onClick={() => setAnchorEl(null)}>Logout</MenuItem>
      </Menu>
    </React.Fragment>
  );
}

export default withStyles(style)(UserButton);
