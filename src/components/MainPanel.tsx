import * as React from 'react';
import { Dispatch, SetStateAction } from 'react';
import {
  createStyles,
  withStyles,
  WithStyles,
  Theme,
} from '@material-ui/core/styles';

import Sidebar from './Sidebar';
import CenterPanel from './CenterPanel';
import TopAppBar from './TopAppBar';

export interface IMainPanelProps extends WithStyles<typeof styles> {
  activeTab: string;
  setActiveTab: Dispatch<SetStateAction<string>>;
  isDark: boolean;
  setDark: Dispatch<SetStateAction<boolean>>;
}

const styles = (theme: Theme) =>
  createStyles({
    root: {
      flex: 4,
      height: '100vh',
      color: theme.palette.primary.main,
      borderRight: `1px solid ${theme.palette.secondary.light}`,
    },
    rows: {
      display: 'flex',
      flex: 1,
      marginBottom: 'auto',
    },
  });

function MainPanel(props: IMainPanelProps): JSX.Element {
  const { classes, activeTab, setActiveTab, isDark, setDark } = props;

  return (
    <div className={classes.root}>
      <TopAppBar activeTab={activeTab} isDark={isDark} setDark={setDark} />
      <div className={classes.rows}>
        <Sidebar activeTab={activeTab} onPress={setActiveTab} />
        <CenterPanel activeTab={activeTab} />
      </div>
    </div>
  );
}

export default withStyles(styles)(MainPanel);
