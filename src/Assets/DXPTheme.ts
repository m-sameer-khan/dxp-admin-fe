import { createMuiTheme } from '@material-ui/core/styles';

export interface IThemeColors {
  primaryColor: string;
  secondaryColor: string;
  backgroundColor: string;
  highlightColor: string;
  cardColor: string;
  itemColor: string;
  borderColor: string;
}

const alpha = (primaryColor: string) =>
  `rgba(${parseInt(primaryColor.substring(1, 3), 16)}, ${parseInt(
    primaryColor.substring(3, 5),
    16,
  )}, ${parseInt(primaryColor.substring(5, 7), 16)}, 0.3)`;

const oko = ({
  primaryColor,
  secondaryColor,
  backgroundColor,
  highlightColor,
  cardColor,
  itemColor,
  borderColor,
}: IThemeColors) => {
  const boxShadowColor = alpha(primaryColor);

  return createMuiTheme({
    overrides: {
      MuiInput: {
        root: {
          fontSize: '24px',
          fontColor: secondaryColor,
        },
        underline: {
          '&:after': {
            // The MUI source seems to use this but it doesn't work
            borderBottom: `2px solid ${secondaryColor}`,
          },
        },
      },
      MuiFormLabel: {
        root: {
          fontSize: '18px',
          color: secondaryColor,
          '&$focused': {
            color: secondaryColor,
          },
        },
      },
      MuiMenuItem: {
        root: {
          background: backgroundColor,
          '&$selected': {
            background: `linear-gradient(90deg, ${highlightColor} 2%, ${backgroundColor} 2%)`,
          },
        },
      },
    },
    palette: {
      primary: {
        main: primaryColor,
        light: boxShadowColor,
        dark: primaryColor,
        contrastText: cardColor,
      },
      secondary: {
        main: secondaryColor,
        light: borderColor,
      },
      error: {
        main: '#f44336',
      },
      background: {
        paper: cardColor,
        default: backgroundColor,
      },
      text: {
        primary: highlightColor,
        secondary: cardColor,
        disabled: 'rgba(0, 0, 0, 0.38)',
        hint: itemColor,
      },
      action: {
        // active: 'rgba(0, 0, 0, 0.54)',
        // hover: 'rgba(0, 0, 0, 0.08)',
        // hoverOpacity: 0.08,
        selected: 'rgba(0, 0, 0, 1)',
        // disabled: 'rgba(0, 0, 0, 0.26)',
        // disabledBackground: 'rgba(0, 0, 0, 0.12)'
      },
    },
    // props: {
    //   MuiPaper: {
    //     elevation: 0
    //   },
    //   MuiButton: {
    //     elevation: 0
    //   }
    // },
    typography: {
      fontFamily: [
        'Lato',
        '-apple-system',
        'BlinkMacSystemFont',
        '"Segoe UI"',
        'Roboto',
        '"Helvetica Neue"',
        'Arial',
        'sans-serif',
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
      ].join(','),
      fontSize: 14,
      fontWeightLight: 300,
      fontWeightRegular: 400,
      fontWeightMedium: 500,
      useNextVariants: true,
      h3: {
        color: 'rgba(0, 0, 0, 0.87)',
        fontWeight: 'bold',
        fontSize: '3rem',
        lineHeight: 1.04,
        letterSpacing: '0em',
      },
      h4: {
        color: 'rgba(0, 0, 0, 0.87)',
        fontWeight: 500,
        fontSize: '3rem',
        lineHeight: 1.04,
        letterSpacing: '0em',
      },
    },
    shape: {
      borderRadius: 2,
    },
  });
};

export default oko;
