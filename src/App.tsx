import * as React from 'react';
import { useState } from 'react';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import {
  CssBaseline,
  withStyles,
  createStyles,
  WithStyles,
} from '@material-ui/core';

import DXPTheme from './Assets/DXPTheme';
import MainPanel from './components/MainPanel';
import Graphs from './components/Graphs';

export interface IAppProps extends WithStyles<typeof style> {}

const primaryColor = '#4c83ff';
const secondaryColor = '#000000';
const backgroundColor = '#f5f6fa';
const highlightColor = '#63d689';
const cardColor = '#ffffff';
const itemColor = '#4b4d54';
const borderColor = '#e7ebf3';

const primaryDarkColor = '#2a3ebf';
const secondaryDarkColor = '#ffffff';
const backgroundDarkColor = '#000000';
const highlightDarkColor = '#86e847';
const cardDarkColor = '#15151c';
const itemDarkColor = '#ffffff';
const borderDarkColor = '#062a63';

// const primaryColor = '#ef6f6f';
// const secondaryColor = '#000000';
// const backgroundColor = '#faf4dc';
// const highlightColor = '#004c99';
// const itemColor = '#4b4d54';
// const cardColor = '#f4c542';
// const borderColor = '#e7ebf3';

const style = createStyles({
  main: {
    display: 'flex',
  },
});

function App(props: IAppProps): JSX.Element {
  const { classes } = props;
  const [activeTab, setActiveTab] = useState('Tenants');
  const [isDark, setDark] = useState(false);

  return (
    <MuiThemeProvider
      theme={
        isDark
          ? DXPTheme({
              primaryColor: primaryDarkColor,
              secondaryColor: secondaryDarkColor,
              backgroundColor: backgroundDarkColor,
              highlightColor: highlightDarkColor,
              cardColor: cardDarkColor,
              itemColor: itemDarkColor,
              borderColor: borderDarkColor,
            })
          : DXPTheme({
              primaryColor,
              secondaryColor,
              backgroundColor,
              highlightColor,
              cardColor,
              itemColor,
              borderColor,
            })
      }
    >
      <CssBaseline />
      <div className={classes.main}>
        <MainPanel
          activeTab={activeTab}
          setActiveTab={setActiveTab}
          isDark={isDark}
          setDark={setDark}
        />
        {activeTab === 'Tenants' ? <Graphs /> : null}
      </div>
    </MuiThemeProvider>
  );
}

export default withStyles(style)(App);
